<?php
/**
 * Footer template file
 *
 * @package Carte Blanche Bourbon
 * @since 2015
 */
?>
	</div>
</div><!-- #main -->
<footer id="colophon">

</footer>
<div class="copyright">
	<div class="container">
		<a href="http://www.chefduweb.nl" title="Website door Chef du Web" class="mustache"></a>
	</div>
</div>

<?php wp_footer(); ?>

<!-- analytics -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X','auto');ga('send','pageview');
</script>
</body>
</html>